# README #
These are some supporting scripts for Atlassian Crowd deployment on Debian Linux.

### What is this repository for? ###

* A sysvinit script for Atlassian Apps (configured for Crowd)
* A script to change the ownership to a 'crowd' user

### How do I get set up? ###

* git clone this repo
* install crowd