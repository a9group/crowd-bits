#!/bin/bash
### BEGIN INIT INFO
# Provides:          crowd
# Required-Start:    $local_fs $remote_fs $network $syslog $named
# Required-Stop:     $local_fs $remote_fs $network $syslog $named
# Should-Start:      mysql
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# X-Interactive:     true
# Short-Description: Start/stop crowd
### END INIT INFO
# Based on script at http://www.bifrost.org/problems.html
# Improved by @alienfast to:
#   - work with any atlassian product based on the location of apache-tomcat
#   - allow for a configurable wait to definitively kill the proces (using catalina.sh's built in method)
#   - added status method that utilizes ps to confidently find a running process
#   - added a confirmation on start to avoid multiple processes
#
APP=crowd
RUN_AS_USER=crowd
KILL_WAIT=30
CATALINA_HOME=/opt/atlassian/$APP/apache-tomcat
#----------------------------------------------------
# Below here stays the same regardless of product
#
if [ -f $CATALINA_HOME/work/$APP.pid ]
then
	PID_FILE=$CATALINA_HOME/work/$APP.pid
	PID_VAR="CATALINA_PID=$PID_FILE"
	VARS="$PID_VAR bash -c"
else
	PID_FILE=""
	PID_VAR=""
	VARS=""
fi
  
start() {
        STATUS=$( ps aux | grep "[c]atalina.base=$CATALINA_HOME" | wc -l )
        if [ $STATUS -gt 0 ]; then
            echo "$APP is ALREADY running. Aborting the start."
        else
            echo "Starting $APP: "
            if [ "x$USER" != "x$RUN_AS_USER" ]; then
              su - $RUN_AS_USER -c "$VARS \"$CATALINA_HOME/bin/startup.sh\""
            else
              $VARS "$CATALINA_HOME/bin/startup.sh"
            fi
            echo "done."
    fi
}
stop() {
        echo "Shutting down $APP: "
        if [ "x$USER" != "x$RUN_AS_USER" ]; then
          su - $RUN_AS_USER -c "$VARS \"$CATALINA_HOME/bin/catalina.sh stop $KILL_WAIT -force\""
        else
          $VARS "$CATALINA_HOME/bin/catalina.sh stop $KILL_WAIT -force"
        fi
        echo "done."
}
  
case "$1" in
   status)
    STATUS=$( ps aux | grep "[c]atalina.base=$CATALINA_HOME" | wc -l )
        if [ $STATUS -gt 0 ]; then
            echo "$APP is running."
        else
            echo "$APP is not running."
        fi
        ;;
  start)
        start
        ;;
  stop)
        stop
        ;;
  restart)
        stop
        start
        ;;
  *)
        echo "Usage: $0 {start|stop|status|restart}"
esac
  
exit 0
