#!/bin/bash
CROWD_USER="crowd"
CROWD_GROUP="crowd"
INSTALL_BASE="/opt/atlassian/atlassian-crowd-2.8.3"
CROWD_HOME="/var/atlassian/crowd"
sudo chgrp ${CROWD_GROUP} ${INSTALL_BASE}/{*.sh,apache-tomcat/bin/*.sh}
sudo chmod g+x ${INSTALL_BASE}/{*.sh,apache-tomcat/bin/*.sh}
sudo chown -R ${CROWD_USER} ${CROWD_HOME} ${INSTALL_BASE}/apache-tomcat/{logs,work,temp}
sudo touch -a ${INSTALL_BASE}/atlassian-crowd-openid-server.log
sudo mkdir ${INSTALL_BASE}/database
sudo chown -R ${CROWD_USER} ${INSTALL_BASE}/{database,atlassian-crowd-openid-server.log}
